<?php

namespace App\DataFixtures;

use App\Entity\Marque;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MarqueFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $marques = ["BMW","Mercedes", "Peugeot", "Renault", "Tesla"];

        foreach ($marques as $marqueName){
            $marque = new Marque();
            $marque->setNom($marqueName);
            $manager->persist($marque);
        }

        $manager->flush();
    }
}
